#!/usr/bin/env python
# coding: utf-8

import json
import logging
import logging.handlers
import paho.mqtt.client as paho
from pprint import pprint as pp
import Adafruit_BBIO.GPIO as GPIO
from config import config as c


class Mqtt:
    """Manage MQTT connection"""

    def __init__(self, config):
        """Create the server connection and recover the config"""
        self.config = config
        self.client = paho.Client()
        self.log = logging.getLogger('mqtt2gpio')
        self.log.setLevel(logging.DEBUG)
        handler = logging.handlers.SysLogHandler(address = '/dev/log')
        self.log.addHandler(handler)
        print('start mqtt2gpio')
        self.log.debug('start mqtt2gpio')
        self.switches = {}
        self.sensors = {}
        self.sensors_queue = {}
        self.client.on_message = self.on_message
        self.client.connect(config.mqtt.server,
                            config.mqtt.port,
                            60)
        self.sub(config.topics.config + '/' + config.client_name)
        self.sub(config.topics.switch)
        self.pub(config.topics.config + '/' + config.client_name, 'get')
        self.loop()

    def on_message(self, mosq, obj, msg):
        """ trigger action when a message is received"""
        self.log.debug("mqtt - new message {} {} {}".format(
            msg.topic, msg.qos, msg.payload))
        (main_topic, sub_topic) = "{}/".format(msg.topic).split('/')[0:2]
        if main_topic == self.config.topics.config and \
                sub_topic == self.config.client_name:
            imported_config = json.loads(msg.payload)
            self.new_setup(imported_config)
        elif main_topic == self.config.topics.switch:
            self.switch(msg.payload)

    def sub(self, topic):
        """ subscribe to a mqtt topic """
        self.log.debug("mqtt - subscribe to '{}'".format(topic))
        self.client.subscribe(topic, 0)

    def pub(self, topic, msg):
        """ publish a message to a topic"""
        self.log.debug("mqtt - publish '{}' to '{}'".format(topic, msg))
        self.client.publish(topic, msg)

    def loop(self):
        """ Loop and wait for action """
        while self.client.loop() == 0:
            for sensor in self.sensors_queue.keys():
                self.sensors_queue[sensor]['cnt'] -= 1
                if self.sensors_queue[sensor]['cnt'] < 1:
                    self.sensor_detect_activate(
                        self.sensors_queue[sensor]['pin'])
                    del(self.sensors_queue[sensor])
            for sensor, pin in self.sensors.iteritems():
                if GPIO.event_detected(pin):
                    self.sensor_trigged(sensor, pin)

    def new_setup(self, new_setup):
        """ Create a new config, and register to new channels"""
        self.clean_setup()
        for elem in new_setup.keys():
            c = new_setup[elem]
            if 'sensor' in c.keys():
                self.add_new_sensor(elem, c['sensor'])
            if 'switch' in c.keys():
                self.add_new_switch(elem, c['switch'])

    def add_new_sensor(self, name, pin):
        """ Add a new sensor and """
        self.sensors[name] = pin
        self.log.debug('sensor - add {}[{}]'.format(name, pin))
        GPIO.setup(pin, GPIO.IN)
        self.sensor_detect_activate(pin)
        self.sensor_trigged(name, pin)

    def sensor_trigged(self, name, pin):
        """ The sensor changed """
        state = GPIO.input(pin)
        GPIO.remove_event_detect(pin)
        self.sensors_queue[name] = {'cnt': 5, 'pin': pin}
        self.log.debug("sensor - set {}[{}] to {}".format(name, pin, state))
        self.pub(self.config.topics.sensor, '{}/{}'.format(name, state))

    def sensor_detect_activate(self, pin):
        """ Activate detection """
        state = GPIO.input(pin)
        edge = GPIO.FALLING if state else GPIO.RISING
        self.log.debug("sensor - activate {} edge to {}".format(pin, state))
        GPIO.add_event_detect(pin, edge)

    def add_new_switch(self, name, pin):
        self.switches[name] = pin
        self.sub(self.config.topics.switch + '/' + name)

    def clean_setup(self):
        """ Remove all the actual config"""
        self.log.debug('config - clean GPIOs')
        self.sensors = {}
        self.switches = {}
        GPIO.cleanup()

    def switch(self, msg):
        """ action a switch """
        (sw, state) = msg.split('/')
        self.log.debug("mqtt - request for {}: {}".format(sw, state))
        if self.is_my_switch(sw):
            self.log.debug('switch - this {} is one of mine'.format(sw))
            state = self.normalize_output(state)
            self.log.debug("switch - action switch {}[{}] set to {}".format(
                sw, self.switches[sw], state))
            GPIO.setup(self.switches[sw], GPIO.OUT)
            GPIO.output(self.switches[sw], state)
        else:
            self.log.debug("switch - {} is not managed here".format(sw))

    def is_my_switch(self, sw):
        """ check if a switch is mine """
        return sw in self.switches.keys()

    def normalize_output(self, state):
        """ normalize a switch action """
        if ((state.lower() in ['on', 'true', 'high'])
                ^ (self.config.switch_inv)):
            return GPIO.HIGH
        return GPIO.LOW


if __name__ == '__main__':
    # create the mqtt connection
    mqtt = Mqtt(c)
